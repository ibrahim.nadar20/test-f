import "./App.css";
import Home from "./pages/Home";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Login from "./pages/Login/Login";
import Register from "./pages/signup/Register";
import React from "react";
import Admin from "./pages/dashboard/Admin";

// const user = localStorage.getItem("token");
// console.log("localstorage: ", user);

function App() {
  // const [currentForm , setCurrentForm]= useState('login');
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route
            path="/home"
            element={
              <>
                <Home />
              </>
            }
          />

          <Route
            path="/login"
            element={
              <>
                {/* { 
            currentForm ==="login" ? <Login /> : <Register />
            } */}
                <Login />
              </>
            }
          />
          <Route
            path="/"
            element={
              <>
                <Register />
              </>
            }
          />
          <Route
            path="/admin"
            element={
              <>
                <Admin />
              </>
            }
          />

          {/* <Route path="/navbar" element={<Navbar />} /> */}
          {/* <Route  path="/" element={<Home />} />
        <Route exact path="/about" component={About} />
        <Route exact path="/contact" component={Contact} />  */}
        </Routes>
      </Router>
    </div>
  );
}

export default App;
