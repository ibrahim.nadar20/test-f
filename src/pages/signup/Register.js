// import * as React from "react";
// import Avatar from "@mui/material/Avatar";
// import Button from "@mui/material/Button";
// import CssBaseline from "@mui/material/CssBaseline";
// import TextField from "@mui/material/TextField";
// import FormControlLabel from "@mui/material/FormControlLabel";
// import Checkbox from "@mui/material/Checkbox";
// import Link from "@mui/material/Link";
// import Grid from "@mui/material/Grid";
// import Box from "@mui/material/Box";
// import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
// import Typography from "@mui/material/Typography";
// import Container from "@mui/material/Container";
// import { createTheme, ThemeProvider } from "@mui/material/styles";
// import { useEffect, useState } from "react";
// import axios from "axios";
// import { useNavigate } from "react-router-dom";

// function Copyright(props) {
//   return (
//     <Typography
//       variant="body2"
//       color="text.secondary"
//       align="center"
//       {...props}
//     >
//       {"Copyright © "}
//       <Link color="inherit" href="https://mui.com/">
//         Your Website
//       </Link>{" "}
//       {new Date().getFullYear()}
//       {"."}
//     </Typography>
//   );
// }

// const theme = createTheme();

// const URLRegister = process.env.REACT_APP_URL + "/users/register";
// export default function SignUp() {
//   const [name, setName] = useState("");
//   const [email, setEmail] = useState("");
//   const [password, setPassword] = useState("");
//   let navigate = useNavigate();

//   // let token = localStorage.getItem("token");

//   const handleSubmit = async (e) => {
//     console.log("before prevent");
//     e.preventDefault();
//     console.log("after prevent");
//     try {
//       const response = await axios.post(`${URLRegister}`, {
//         email: email,
//         password: password,
//       });
//       // console.log("email ", email);
//       // console.log("password ", password);

//       const res = response.data;
//       console.log(res);
//       localStorage.setItem("token", res.token);
//       navigate("/login");
//     } catch (error) {
//       console.log(error);
//     }

//     // const data = new FormData(e.currentTarget);
//     // console.log({
//     //   email: data.get('email'),
//     //   password: data.get('password'),
//     // });
//   };
//   const user = localStorage.getItem("token");

//   useEffect(() => {
//     if (user) navigate("/home");
//   }, []);

//   return (
//     <ThemeProvider theme={theme}>
//       <Container component="main" maxWidth="xs">
//         <CssBaseline />
//         <Box
//           sx={{
//             marginTop: 8,
//             display: "flex",
//             flexDirection: "column",
//             alignItems: "center",
//           }}
//         >
//           <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
//             <LockOutlinedIcon />
//           </Avatar>
//           <Typography component="h1" variant="h5">
//             Sign up
//           </Typography>
//           <Box
//             component="form"
//             noValidate
//             onSubmit={(e)=>handleSubmit}
//             sx={{ mt: 3 }}
//           >
//             <Grid container spacing={2}>
//               <Grid item xs={12} sm={6}>
//                 <TextField
//                   autoComplete="given-name"
//                   name="name"
//                   required
//                   fullWidth
//                   id="name"
//                   label=" Name"
//                   autoFocus
//                   onChange={(e) => {
//                     setName(e.target.value);
//                   }}
//                 />
//               </Grid>

//               <Grid item xs={12}>
//                 <TextField
//                   required
//                   fullWidth
//                   id="email"
//                   label="Email Address"
//                   name="email"
//                   autoComplete="email"
//                   onChange={(e) => {
//                     setEmail(e.target.value);
//                   }}
//                 />
//               </Grid>
//               <Grid item xs={12}>
//                 <TextField
//                   required
//                   fullWidth
//                   name="password"
//                   label="Password"
//                   type="password"
//                   id="password"
//                   autoComplete="new-password"
//                   onChange={(e) => {
//                     setPassword(e.target.value);
//                   }}
//                 />
//               </Grid>
//               <Grid item xs={12}>
//                 <FormControlLabel
//                   control={
//                     <Checkbox value="allowExtraEmails" color="primary" />
//                   }
//                   label="I want to receive inspiration, marketing promotions and updates via email."
//                 />
//               </Grid>
//             </Grid>
//             <Button
//               type="submit"
//               fullWidth
//               variant="contained"
//               sx={{ mt: 3, mb: 2 }}
//               // onClick={handleSubmit}

//             >
//               Sign Up
//             </Button>
//             <Grid container justifyContent="flex-end">
//               <Grid item>
//                 <Link href="#" variant="body2">
//                   Already have an account? Sign in
//                 </Link>
//               </Grid>
//             </Grid>
//           </Box>
//         </Box>
//         <Copyright sx={{ mt: 5 }} />
//       </Container>
//     </ThemeProvider>
//   );
// }

// //  import React, { useEffect, useState } from "react";
// // import { AiFillEye, AiFillEyeInvisible } from "react-icons/ai";
// // import axios from "axios";
// // import URL from "../../helpers/Url";
// // import './register.css'
// // const Register = ({ setAdd, fetchAdmins }) => {
// //   const [name, setName] = useState("");
// //   const [email, setEmail] = useState("");
// //   const [password, setPassword] = useState("");
// //   const [conPassword, setConPassword] = useState("");
// //   const [type, setType] = useState("password");
// //   const [confirmType, setConfirmType] = useState("password");
// //   const [passError, setPassError] = useState(false);
// //   const [errors, setErrors] = useState("");

// //   let token = localStorage.getItem("token");

// //   const Register = async (e) => {
// //     e.preventDefault();
// //     const formData = new FormData(); //new object, bdak form t7t
// //     formData.append("name", name); // append -> la7ata tda5el 3l object
// //     formData.append("email", email);
// //     formData.append("password", password);

// //     const response = await axios.post(`${URL}/api/register`, {
// //       headers: {
// //         Authorization: `Bearer ${token}`,
// //       },
// //     });
// //     if (response.data.success) {
// //       setAdd(false);

// //     } else {
// //       setErrors(response.data[0]);
// //     }
// //   };

// //   const toggllePassword = () => {
// //     type === "password" ? setType("text") : setType("password");
// //   };

// //   const togglleConfirmPassword = () => {
// //     confirmType === "password"
// //       ? setConfirmType("text")
// //       : setConfirmType("password");
// //   };

// //   const confirmPassword = () => {
// //     if (password != conPassword) setPassError(true);
// //     else setPassError(false);
// //   };

// //   useEffect(() => {
// //     if (conPassword != "") confirmPassword();
// //   }, [confirmPassword]);
// //   return (
// //     <div>
// //       <form className="add-admin">
// //         <h3>Add Admin</h3>
// //         <label>
// //           <h3>Name</h3>
// //           <input
// //             type="text"
// //             defaultValue={name}
// //             onChange={(e) => setName(e.target.value)}
// //           ></input>
// //           {errors.name && <div className="passError">{errors.name[0]}</div>}
// //         </label>
// //         <label>
// //           <h3>Email</h3>
// //           <input
// //             type="email"
// //             defaultValue={email}
// //             onChange={(e) => setEmail(e.target.value)}
// //           ></input>
// //           {errors.email && <div className="passError">{errors.email[0]}</div>}
// //         </label>

// //         <label>
// //           <h3>Password</h3>
// //           <input
// //             type={type}
// //             onChange={(e) => setPassword(e.target.value)}
// //           ></input>
// //           <div onClick={toggllePassword} className="passIcon">
// //             {type === "password" ? (
// //               <AiFillEye style={{ color: "grey" }} />
// //             ) : (
// //               <AiFillEyeInvisible style={{ color: "grey" }} />
// //             )}
// //           </div>
// //           {errors.password && (
// //             <div className="passError">{errors.password[0]}</div>
// //           )}
// //         </label>
// //         <label>
// //           <h3>Confirm Password</h3>
// //           <input
// //             type={confirmType}
// //             onChange={(e) => setConPassword(e.target.value)}
// //           ></input>
// //           <div onClick={togglleConfirmPassword} className="passIcon">
// //             {confirmType === "password" ? (
// //               <AiFillEye style={{ color: "grey" }} />
// //             ) : (
// //               <AiFillEyeInvisible style={{ color: "grey" }} />
// //             )}
// //           </div>
// //           {passError && (
// //             <div className="passError">Passwords don't Confirm</div>
// //           )}
// //         </label>
// //         <button onClick={(e) => Register(e)}>Sign up </button>
// //       </form>
// //     </div>
// //   );
// // };

// // export default Register;

import React, { useState, useEffect } from "react";
import "./register.css";
import { Button, Box, TextField, Alert } from "@mui/material";
import { useNavigate } from "react-router-dom";
import kingco from "../../Assets/gi.png";

const Register = () => {
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [generalError, setGeneralError] = useState("");
  const [emailError, setEmailError] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const navigate = useNavigate();

  async function login(e) {
    e.preventDefault();
    setGeneralError("");
    setEmailError("");
    setPasswordError("");

    try {
      let item = { name, email, password };
      let result = await fetch("http://localhost:5000/api/users/register", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(item),
      });
      localStorage.setItem("token", JSON.stringify(result.token));
      result = await result.json();
      console.log(result.token);
      if (result.message) {
        setGeneralError(result.message);
      } else {
        localStorage.setItem("token", JSON.stringify(result.token));
        localStorage.setItem("id", JSON.stringify(result._id));
        return navigate("/login");
      }
    } catch (err) {
      console.log(err);
    }
  }

  return (
    <div className="loginContainer">
      <Box
        component="form"
        sx={{
          "& .MuiTextField-root": { m: 1, width: "25ch" },
        }}
        noValidate
        autoComplete="off"
      >
        <div className="loginContainerform">
          {generalError && (
            <Alert
              className="err"
              severity="error"
              sx={{
                height: "60px",
                transform: "scale(1.3)",
                width: "16rem",
                position: "absolute",
                bottom: "33rem",
                boxShadow: "8",
              }}
            >
              {generalError}
            </Alert>
          )}
          {/* <h1>Votting App</h1> */}
          <img src={kingco} className="be2" />
          <TextField
            onChange={(e) => {
              setName(e.target.value);
              setGeneralError("");
            }}
            className="input1"
            id="outlined-basic"
            label="Full Name"
            error={generalError}
            helperText={emailError}
            required
            variant="outlined"
            value={name}
          />
          <TextField
            onChange={(e) => {
              setEmail(e.target.value);
              setGeneralError("");
            }}
            className="input2"
            id="outlined-basic"
            label="Email ..@.."
            error={generalError}
            helperText={emailError}
            required
            variant="outlined"
            value={email}
          />
          <TextField
            onChange={(e) => {
              setPassword(e.target.value);
              setGeneralError("");
            }}
            value={password}
            className="input3"
            id="outlined-basic"
            label="Password"
            variant="outlined"
            required
            type="password"
            error={generalError}
            helperText={passwordError}
          />
          <Button
            className="loginBtn"
            onClick={login}
            variant="outlined"
            sx={{
              height: "4vh",
              width: "10vw",
              marginTop: "7px",
              fontWeight: "600",
              color: "black",
              backgroundColor: "#CC9900",
            }}
          >
            Sign Up
          </Button>
          <div className="notMemberBtn" onClick={() => navigate("/login")}>
            Already a member?{" "}
            <span className="notloggedBtn" onClick={() => navigate("/login")}>
              Log in
            </span>
            .
          </div>
        </div>
      </Box>
      {/* <div className="footer">Kingco media</div> */}
    </div>
  );
};

export default Register;
