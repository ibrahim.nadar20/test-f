// import * as React from 'react';
// import Avatar from '@mui/material/Avatar';
// import Button from '@mui/material/Button';
// import CssBaseline from '@mui/material/CssBaseline';
// import TextField from '@mui/material/TextField';
// import FormControlLabel from '@mui/material/FormControlLabel';
// import Checkbox from '@mui/material/Checkbox';
// import Link from '@mui/material/Link';
// import Grid from '@mui/material/Grid';
// import Box from '@mui/material/Box';
// import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
// import Typography from '@mui/material/Typography';
// import Container from '@mui/material/Container';
// import { createTheme, ThemeProvider } from '@mui/material/styles';
// import axios from "axios";
// import  { useEffect, useState } from "react";
// import { useNavigate } from "react-router-dom";

// function Copyright(props) {
//   return (
//     <Typography variant="body2" color="text.secondary" align="center" {...props}>
//       {'Copyright © '}
//       <Link color="inherit" href="https://mui.com/">
//         Your Website
//       </Link>{' '}
//       {new Date().getFullYear()}
//       {'.'}
//     </Typography>
//   );
// }

// const theme = createTheme();

// const URLLogin = process.env.REACT_APP_URL + "/users/login";

// const Login = async (e) => {
//   const [password, setPassword] = useState("");
//   const [email, setEmail] = useState("");
//   // const [generalError, setGeneralError] = useState("");
//   // const [emailError, setEmailError] = useState("");
//   // const [passwordError, setPasswordError] = useState("");
//   // const [error, setError] = useState(false);
//   let navigate = useNavigate();
//   const [type, setType] = useState("password");

//   console.log("before prevent");
//   e.preventDefault();
//   console.log("after prevent");

//   try {
//     const response = await axios.post(`${URLLogin}`, {
//       email: email,
//       password: password,
//     });
//     // console.log("email ", email);
//     // console.log("password ", password);

//     const res = response.data;
//     console.log(res);
//     localStorage.setItem("token", res.token);
//     navigate("/home");
//   } catch (error) {
//     console.log(error);
//   }

// const user = localStorage.getItem("token");

// useEffect(() => {
//   if (user) navigate("/");
// }, []);

// // export default function Login() {
// //   const handleSubmit = (event) => {
// //     event.preventDefault();
// //     const data = new FormData(event.currentTarget);
// //     console.log({
// //       email: data.get('email'),
// //       password: data.get('password'),
// //     });
// //   };

//   return (
//     <ThemeProvider theme={theme}>
//       <Container component="main" maxWidth="xs">
//         <CssBaseline />
//         <Box
//           sx={{
//             marginTop: 8,
//             display: 'flex',
//             flexDirection: 'column',
//             alignItems: 'center',
//           }}
//         >
//           <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
//             <LockOutlinedIcon />
//           </Avatar>
//           <Typography component="h1" variant="h5">
//             Sign in
//           </Typography>
//           <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
//           {/* <Box component="form"   noValidate sx={{ mt: 1 }}> */}
//             <TextField
//               margin="normal"
//               required
//               fullWidth
//               id="email"
//               label="Email Address"
//               name="email"
//               autoComplete="email"
//               autoFocus
//               // onChange={(e) => {
//               //     setEmail(e.target.value);

//               //   }}
//              />
//             <TextField
//               margin="normal"
//               required
//               fullWidth
//               name="password"
//               label="Password"
//               type="password"
//               id="password"
//               autoComplete="current-password"
//               // onChange={(e) => {
//               //   setPassword(e.target.value);
//               // }}
//             />
//             <FormControlLabel
//               control={<Checkbox value="remember" color="primary" />}
//               label="Remember me"
//             />
//             <Button
//               type="submit"
//               // onClick={Login}
//               fullWidth
//               variant="contained"
//               sx={{ mt: 3, mb: 2 }}
//             >
//               Sign In
//             </Button>
//             <Grid container>
//               <Grid item xs>
//                 <Link href="#" variant="body2">
//                   Forgot password?
//                 </Link>
//               </Grid>
//               <Grid item>
//                 <Link href="#" variant="body2">
//                   {"Don't have an account? Sign Up"}
//                 </Link>
//               </Grid>
//             </Grid>
//           </Box>
//         </Box>
//         <Copyright sx={{ mt: 8, mb: 4 }} />
//       </Container>
//     </ThemeProvider>
//   );
// }

import React, { useEffect, useState } from "react";
import "./login.css";
import { useNavigate } from "react-router-dom";
import { Button, Box, TextField, Alert } from "@mui/material";
import axios from "axios";
// import URL from "../../helpers/Url";
import kingco from "../../Assets/gi.png";

const URLLogin = process.env.REACT_APP_URL + "/users/login";

const Login = () => {
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [generalError, setGeneralError] = useState("");
  const [emailError, setEmailError] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [error, setError] = useState(false);
  let navigate = useNavigate();
  const [type, setType] = useState("password");

  const loginFunc = async (e) => {
    console.log("before prevent");
    e.preventDefault();
    setGeneralError("");
    setEmailError("");
    setPasswordError("");
    // console.log("after prevent");

    try {
      const response = await axios.post(`${URLLogin}`, {
        email: email,
        password: password,
      });
      // console.log("email ", email);
      // console.log("password ", password);

      const res = response.data;
      console.log("hii", res);
      localStorage.setItem("token", res.token);

      navigate("/home");
      console.log("hello", error);
    } catch (error) {
      console.log(error);
    }
  };

  const user = localStorage.getItem("token");
  //kermal bas yerj3 bl sahem lawara ydal bl home page (security)
  // useEffect(() => {
  //   if (user) navigate("/home");
  // }, []);

  return (
    <div className="loginContainer2">
      <Box
        component="form"
        sx={{
          "& .MuiTextField-root": { m: 1, width: "25ch" },
        }}
        noValidate
        autoComplete="off"
      >
        <div className="loginContainerform2">
          {generalError && (
            <Alert
              severity="error"
              sx={{
                height: "40px",
                transform: "scale(1.3)",
                width: "10rem",
                position: "absolute",
                top: "20%",
              }}
            >
              {generalError}
            </Alert>
          )}

          <img src={kingco} className="be3" alt="Gi" />
          {/* <h2>Please enter your email and password.</h2> */}
          <TextField
            onChange={(e) => {
              setEmail(e.target.value);
              setGeneralError("");
            }}
            className="input2"
            id="outlined-basic"
            label="Email"
            error={generalError}
            helperText={emailError}
            value={email}
            required
            variant="outlined"
          />
          <TextField
            onChange={(e) => {
              setPassword(e.target.value);
              setGeneralError("");
            }}
            value={password}
            className="input2"
            id="outlined-basic"
            label="Password"
            variant="outlined"
            required
            type="password"
            error={generalError}
            helperText={passwordError}
          />
          <Button
            className="loginBtn2"
            onClick={loginFunc}
            type="submit"
            variant="outlined"
            sx={{
              height: "6vh",
              width: "10vw",
              marginTop: "10px",
              fontWeight: "600",
              color: "var(--white)",
              backgroundColor: "var(--black)",
            }}
          >
            Submit
          </Button>
        </div>
      </Box>
    </div>
  );
};

export default Login;

//  import React, { useEffect, useState } from "react";
// import { AiFillEye, AiFillEyeInvisible } from "react-icons/ai";
// import axios from "axios";
// import URL from "../../helpers/Url";
// import './register.css'
// const Register = ({ setAdd, fetchAdmins }) => {
//   const [name, setName] = useState("");
//   const [email, setEmail] = useState("");
//   const [password, setPassword] = useState("");
//   const [conPassword, setConPassword] = useState("");
//   const [type, setType] = useState("password");
//   const [confirmType, setConfirmType] = useState("password");
//   const [passError, setPassError] = useState(false);
//   const [errors, setErrors] = useState("");

//   let token = localStorage.getItem("token");

//   const Register = async (e) => {
//     e.preventDefault();
//     const formData = new FormData(); //new object, bdak form t7t
//     formData.append("name", name); // append -> la7ata tda5el 3l object
//     formData.append("email", email);
//     formData.append("password", password);

//     const response = await axios.post(`${URL}/api/register`, {
//       headers: {
//         Authorization: `Bearer ${token}`,
//       },
//     });
//     if (response.data.success) {
//       setAdd(false);

//     } else {
//       setErrors(response.data[0]);
//     }
//   };

//   const toggllePassword = () => {
//     type === "password" ? setType("text") : setType("password");
//   };

//   const togglleConfirmPassword = () => {
//     confirmType === "password"
//       ? setConfirmType("text")
//       : setConfirmType("password");
//   };

//   const confirmPassword = () => {
//     if (password != conPassword) setPassError(true);
//     else setPassError(false);
//   };

//   useEffect(() => {
//     if (conPassword != "") confirmPassword();
//   }, [confirmPassword]);
//   return (
//     <div>
//       <form className="add-admin">
//         <h3>Add Admin</h3>
//         <label>
//           <h3>Name</h3>
//           <input
//             type="text"
//             defaultValue={name}
//             onChange={(e) => setName(e.target.value)}
//           ></input>
//           {errors.name && <div className="passError">{errors.name[0]}</div>}
//         </label>
//         <label>
//           <h3>Email</h3>
//           <input
//             type="email"
//             defaultValue={email}
//             onChange={(e) => setEmail(e.target.value)}
//           ></input>
//           {errors.email && <div className="passError">{errors.email[0]}</div>}
//         </label>

//         <label>
//           <h3>Password</h3>
//           <input
//             type={type}
//             onChange={(e) => setPassword(e.target.value)}
//           ></input>
//           <div onClick={toggllePassword} className="passIcon">
//             {type === "password" ? (
//               <AiFillEye style={{ color: "grey" }} />
//             ) : (
//               <AiFillEyeInvisible style={{ color: "grey" }} />
//             )}
//           </div>
//           {errors.password && (
//             <div className="passError">{errors.password[0]}</div>
//           )}
//         </label>
//         <label>
//           <h3>Confirm Password</h3>
//           <input
//             type={confirmType}
//             onChange={(e) => setConPassword(e.target.value)}
//           ></input>
//           <div onClick={togglleConfirmPassword} className="passIcon">
//             {confirmType === "password" ? (
//               <AiFillEye style={{ color: "grey" }} />
//             ) : (
//               <AiFillEyeInvisible style={{ color: "grey" }} />
//             )}
//           </div>
//           {passError && (
//             <div className="passError">Passwords don't Confirm</div>
//           )}
//         </label>
//         <button onClick={(e) => Register(e)}>Sign up </button>
//       </form>
//     </div>
//   );
// };

// export default Register;
