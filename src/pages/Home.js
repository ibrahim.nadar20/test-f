import React from "react";
import "./home.css";

import Footer from "../components/footer/Footer";
import Video from "../components/video/Video";
import Logos from "../components/logos/Logos";
import Card from "../components/cards/Card";
import Navbar from "../components/navbar/Navbar";


let token = localStorage.getItem("token");

function Home() {
  return (
    <>
      <Navbar />
      <Video />
      <Logos />
      <Card />
      <Footer />
    </>
  );
}

export default Home;
