import React from "react";
import "./footer.css";
import gi from "../../Assets/gi.png";
function Footer() {
  return (
    <footer className="footer-distributed">
      <div className="footer-left">
        {/* <h3>
          Company<span>logo</span>
        </h3> */}
        <a
           href="https://www.instagram.com/golden.international2022/?igshid=YmMyMTA2M2Y%3D"
        style={{ target: "blank"  }}
      >
        <img src={gi} className="logo-f" alt="3anbar_restaurant" />
      </a>

        {/* <p className="footer-links">
          <a href="#" className="link-1">
            Home
          </a>

          <a href="#">Blog</a>

          <a href="#">Pricing</a>

          <a href="#">About</a>

          <a href="#">Faq</a>

          <a href="#">Contact</a>
        </p> */}
        <br />
        <br />
        <a href="https://www.instagram.com/golden.international2022/?igshid=YmMyMTA2M2Y%3D">
          <p className="footer-company-name">Golden_International© 2015</p> 
          </a>
      </div>

      <div className="footer-center">
        <div>
          <i className="fa fa-map-marker"></i>
          <p>
            <span> Koraytem Street</span>Beirut-Lebanon
          </p>
        </div>

        <div>
          <i className="fa fa-phone"></i>
          <p>+961 81 070 009</p>
          <br />
          <br /> <br />
          <br />
        </div>

        <div>
          <i className="fa fa-envelope"></i>
          <p>
            <a href="https://www.instagram.com/kingco.socialmedia/?hl=en">
              Powered by KinGco Media
            </a>
          </p>
        </div>
      </div>

      <div className="footer-right">
        <div className="footer-center">
          <div>
            <i className="fa fa-map-marker"></i>
            <p>
              <span>Golden Int.</span>
              <span>KinGco media</span> Anbar Rest. <span>Khabar Ajel</span>
            </p>
          </div>

          <div>
            <i className="fa fa-phone"></i>
            {/* <p>+961 81 070 009</p> */}
            {/* <br /><br /> */}
          </div>

          <div>
            <i className="fa fa-envelope"></i>
            <p>
              <a
                href="mailto:info@kingco.media"
              >
                info@kingco.media
              </a>
            </p>
          </div>
        </div>
        {/* <p className="footer-company-about">
        <span>About the company</span>
        We are driven by creativity. We create innovative things to help you
        achieve better results and consolidate yourself in the market. We do
        unique designs.
      </p>

      <div className="footer-icons">
        <a href="#"><i className="fa fa-facebook"></i></a>
        <a href="#"><i className="fa fa-twitter"></i></a>
        <a href="#"><i className="fa fa-linkedin"></i></a>
        <a href="#"><i className="fa fa-github"></i></a>
      </div>   */}
      </div>
    </footer>
  );
}

export default Footer;
