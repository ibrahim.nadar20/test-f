import React from "react";
import "./card.css";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { ToastContainer, react_toastify, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
// import * as qs from "qs";


const URLCards = process.env.REACT_APP_URL + "/inf";
// const URLVote = process.env.REACT_APP_URL + "/users/vote";

function Card() {
  let token = localStorage.getItem("token");

  const [influencers, setInfluencers] = useState([]);
  const [id_inf, setId_inf] = useState("");
  // const [message, setMessage] = useState(false);
  // const [error, setError] = useState(false);

  useEffect(() => {
    getInfluencers();
  }, []);

  const getInfluencers = async () => {
    // const {  image } = props;
    try {
      const response = await axios.get(`${URLCards}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      setInfluencers(response.data);
      // console.log(response.data);
    } catch (err) {
      // console.log('er',err);
    }
    console.log(influencers);
  };

  // const [errors, setErrors] = useState("");

  const Vote = async (e, id_inf) => {
    // const data = { id_inf};
    let token = localStorage.getItem("token");
    e.preventDefault();

    // fetch('http://localhost:5000/api/users/vote', {
    //   method: 'POST',
    //   headers: {
    //     'Content-Type': 'application/json',
    //     Authorization: `Bearer ${token}`,
    //   },
    //   body: JSON.stringify(data),
    // })
    // .then((response) => response.json())
    // //Then with the data from the response in JSON...
    // .then((data) => {
    //   console.log('Success:', data);
    // })
    // //Then with the error genereted...
    // .catch((error) => {
    //   console.error('Error:', error);
    // });

    (async () => {
      const rawResponse = await fetch("http://localhost:5000/api/users/vote", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({ id_inf }),
        // body:id_inf
      });
      const content = await rawResponse.json();
      alert(JSON.stringify(content));
      // showToastMessage(JSON.stringify(content));
      // console.log(content);
      
    })
    
    (
       

    );

    // fetch(`http://localhost:5000/api/users/vote`, {
    //   method: "POST",
    //   headers: {
    //     "Content-Type": "application/x-www-form-urlencoded;",
    //     'accept': 'application/json',
    //     Authorization: `Bearer ${token}`,
    //     data: qs.stringify({

    //       id_inf:id_inf,

    //       }),
    //     // body: {
    //     //   id_inf:id_inf,
    //     // },
    //   },

    // })
    // .then((response) => response.json())
    // .then((responseData) => {
    //   alert(JSON.stringify(responseData));
    // });

    //     const bodyData = JSON.stringify({
    //       id_inf
    //   });
    //   try {
    //     const response = await fetch(`http://localhost:5000/api/users/vote`, {
    //         method: "POST", body: bodyData, headers: {
    //           Authorization: `Bearer ${token}`,
    //             'content-type':'application/x-www-form-urlencoded; charset=UTF-8',
    //             'accept': 'application/json'
    //         }
    //     });
    //     const res = await response.json()
    //     console.log(res)
    // }
    // catch (err) {
    //     console.log(err);
    // }

    // The original

    // const formData = new FormData();
    // formData.append("id_inf", id_inf);
    // const response = await axios.post(
    //   `http://localhost:5000/api/users/vote `,
    //   {
    //     formData,
    //     headers: {
    //       Authorization: `Bearer ${token}`,
    //       "Content-Type": "application/x-www-form-urlencoded;",
    //       'accept': 'application/json',
    //     },

    //   }
    // );

    // if (response.data) {
    //   showToastMessage("hou have voted successfully !");
    // } else {
    //   setErrors(response.data[0]);
    //   // setErrors(response.data.message);
    // }
    // console.log("hellooo", setErrors);
  };

  const showToastMessage = () => {
    toast.success("Success Notification !", {
      position: toast.POSITION.TOP_RIGHT,
    });
  };

  const navigate = useNavigate();
  useEffect(() => {
    if (!localStorage.getItem("token")) {
      navigate("/login");
    }
  });

  return (
    <>
      <div className="main">
        <h1 key={influencers._id}>{influencers.name}</h1>
        <h1 className="h1">Our Influencers</h1>
        <h3 className="h6">You can vote only once !!!</h3>
        {/* <button onClick={showToastMessage}>Notify</button> */}
        <ToastContainer />
        <ul className="cards">
          {influencers.map((influencer) => (
            <li key={influencer._id} className="cards_item">
              <div className="card">
                <div className="card_image">
                  <img
                    key={influencer._id}
                    // src={influencer.image[0]}
                    src={`http://localhost:5000/${influencer.image}`}
                    alt="kingco"
                  />
                </div>
                <div className="card_content">
                  <h2 className="card_title">{influencer.name}</h2>
                  <p className="card_text">
                    Vote now online for your Best Influencer !<br></br>{" "}
                    <br></br>
                  </p>
                  <button
                    onClick={(e) => Vote(e, influencer._id)}
                    value={setId_inf}
                    // onChange={(e) => setInfPerson(e.target.value)}
                    onChange={(e) => setId_inf(e.target.value)}
                    className="btn_c card_btn"
                  >
                    Vote
                  </button>
                </div>
              </div>
            </li>
          ))}
        </ul>
      </div>

      <h3 className="made_by">Made with ♡</h3>
    </>
  );
}

export default Card;
